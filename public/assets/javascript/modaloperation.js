
function myfunction(){
    jQuery('#modal-add-user').modal('show');
}
function deleteUser(){
    jQuery('#modal-delete-user').modal('show');
}
function addloginUser(){
    
}
jQuery(document).ready(function($){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#addbtn").click(function(){
        email = $("#emailadd").val();
        password = $("#passwordadd").val();
        role = $("#roletype").val();
        clearfield();
        // alert(role);
        var data = {
            "email":email,
            "password":password,
            "role":role
        }
        $.ajax({
            url: baseUrl+"/addUser",
            type:"POST",
            dataType:"json",
            data:data,
            success:function(response){
                if(response.success != null){
                    // alert(response.success);
                    $("#modal-add-user").modal('hide');
                    location.reload();
                }
                else{
                    // alert(response.error);
                }
                
            },
            error:function(){
                // alert("error happened");
            }
        });
    });
    $("#addloginManager").click(function () {
        name = $("#username").val();
        password = $("#userpassword").val();
        geo = $("#usergeo").val();
        clearfield();
        // alert(role);
        var data = {
            "name": name,
            "password": password,
            "geo": geo
        }
        $.ajax({
            url: baseUrl + "/addloginManager",
            type: "POST",
            dataType: "json",
            data: data,
            success: function (response) {
                if (response.success != null) {
                    // alert(response.success);
                    $("#modal-add-loginuser").modal('hide');
                    location.reload();
                }
                else {
                    // alert(response.error);
                }

            },
            error: function () {
                // alert("error happened");
            }
        });
    });
    $("#addtrackManager").click(function () {
        tracktitle = $("#tracktitle").val();
        url = $("#urlname").val();
        clearfield();
        // alert(role);
        var data = {
            "tracktitle": tracktitle,
            "url": url
        }
        $.ajax({
            url: baseUrl + "/addtrackManager",
            type: "POST",
            dataType: "json",
            data: data,
            success: function (response) {
                if (response.success != null) {
                    // alert(response.success);
                    $("#modal-add-trackmanager").modal('hide');
                    location.reload();
                }
                else {
                    // alert(response.error);
                }

            },
            error: function () {
                // alert("error happened");
            }
        });
    });

    $("#addplaylistManager").click(function () {
        playtitle = $("#playtitle").val();
        playurl = $("#playurl").val();
        clearfield();
        // alert(role);
        var data = {
            "playtitle": playtitle,
            "playurl": playurl
        }
        $.ajax({
            url: baseUrl + "/addplaylistManager",
            type: "POST",
            dataType: "json",
            data: data,
            success: function (response) {
                if (response.success != null) {
                    // alert(response.success);
                    $("#modal-add-playlistmanager").modal('hide');
                    location.reload();
                }
                else {
                    // alert(response.error);
                }

            },
            error: function () {
                // alert("error happened");
            }
        });
    });
});
function clearfield(){
    jQuery(".form-horizontal:input").each(function(){
        $(this).val('');
    });
}

function deleteUser(){   
    var checkedUser = $("#user-table input[type='checkbox']:checked");  //get the checked ids
    var ids = []; //the checked id array

    checkedUser.each(function(index){
        ids.push($(this).val());    
    });

    $.ajax({
        url:baseUrl + '/deleteUser',
        type:"post",
        data:{
            ids:ids
        },
        success:function(response){
            location.reload();
        },
        error:function(error){

        }
    });
}
var idval;
function getVal(val){
    console.log(val);
    //idval = $(".modaledit").attr("id")
    idval = val;
}
function editUser(){
    email = $("#email").val();
    password = $("#password").val();
    role = $("#profile-role-type").val();
    id = idval;
    var data = {
        "id":id,
        "email": email,
        "password": password,
        "role": role
    }
    $.ajax({
        url:baseUrl + '/editUser',
        type:"POST",
        dataType:"json",
        data:data,
        success:function(response){
            if (response.success != null){
                location.reload();
            }
        },
        error:function(error){

        }
        
    });
}

function deleteManager() {
    var checkedUser = $("#user-table input[type='checkbox']:checked");  //get the checked ids
    var ids = []; //the checked id array

    checkedUser.each(function (index) {
        ids.push($(this).val());
    });

    $.ajax({
        url: baseUrl + '/deleteManager',
        type: "post",
        data: {
            ids: ids
        },
        success: function (response) {
            location.reload();
        },
        error: function (error) {

        }
    });
}
function updateManager() {
        state = $("#layout-subnavbar-style-state").val();
        service = $("#layout-subnavbar-style-service").val();
        id = idval;
        var data = {
            "id": id,
            "state": state,
            "service": service
        }
        $.ajax({
            url: baseUrl + '/updateManager',
            type: "POST",
            dataType: "json",
            data: data,
            success: function (response) {
                if (response.success != null) {
                    location.reload();
                }
            },
            error: function (error) {

            }

        });
}
function deletetrackManager() {
    var checkedUser = $("#user-table input[type='checkbox']:checked");  //get the checked ids
    var ids = []; //the checked id array

    checkedUser.each(function (index) {
        ids.push($(this).val());
    });

    $.ajax({
        url: baseUrl + '/deletetrackManager',
        type: "post",
        data: {
            ids: ids
        },
        success: function (response) {
            location.reload();
        },
        error: function (error) {

        }
    });
}

function deleteplaylistManager() {
    var checkedUser = $("#user-table input[type='checkbox']:checked");  //get the checked ids
    var ids = []; //the checked id array

    checkedUser.each(function (index) {
        ids.push($(this).val());
    });

    $.ajax({
        url: baseUrl + '/deleteplaylistManager',
        type: "post",
        data: {
            ids: ids
        },
        success: function (response) {
            location.reload();
        },
        error: function (error) {

        }
    });
}