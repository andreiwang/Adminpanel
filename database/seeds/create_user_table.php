<?php

use Illuminate\Database\Seeder;
use App\User;

class Create_user_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $userOne = new User();

        $userOne->id = 1;
        $userOne->email = "toru@mail.com";
        $userOne->password = bcrypt("toru123");
        $userOne->role = "Admin";
        $userOne->save();
    }
}
