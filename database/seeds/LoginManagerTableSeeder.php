<?php

use Illuminate\Database\Seeder;
use App\app\loginmanager;
class LoginManagerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $manager = new loginmanager();

        $manager->id = 1;
        $manager->name = "toru";
        $manager->password = bcrypt("toru123");
        $manager->geo = "Russia";
        $manager->service = "";
        $manager->state = "";
        $manager->save();
    }
}
