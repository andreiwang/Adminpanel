<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(create_user_table::class);
        // $this->call(LoginManagerTableSeeder::class);
        // $this->call(TrackManagerTableSeeder::class);
         $this->call(PlayManagerTableSeeder::class);
    }
}
