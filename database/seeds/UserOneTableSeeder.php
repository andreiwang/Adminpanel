<?php

use Illuminate\Database\Seeder;
use App\user;

class UserOneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $userOne = new user();

        $userOne->id = 1;
        $userOne->email = "toru@mail.com";
        $userOne->password = bcrypt("toru123");

        $userOne->save();
    }
}
