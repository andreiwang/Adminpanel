
    <!-- Bower Libraries Styles -->
    <link rel="stylesheet" href="{{URL::to('/assets/vendor/css/lib.min.css')}}">

    <script src="{{URL::to('/assets/javascript/app.min.13a3a368.js')}}"></script>

    <script src="{{URL::to('/assets/javascript/plugins-init.js')}}"></script>
    <script src="{{URL::to('/assets/javascript/switchery-settings.js')}}"></script>