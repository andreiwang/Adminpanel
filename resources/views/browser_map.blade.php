@extends('layouts.dashboard')

@push('style2')
<!-- <link href="{{URL::to('/')}}/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/> -->

<!-- <link href="{{URL::to('/')}}/vendors/bower_components/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css"/>		 -->
<link href="{{URL::to('/')}}/dist/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/dist/css/material-design-iconic-font.min.css" rel="stylesheet" type="text/css"/>
<link href="{{URL::to('/')}}/vendors/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" type="text/css"/>
<!-- <link href="{{URL::to('/')}}/src/scss/style.scss}}" rel="stylesheet" type="text/css"/> -->
<!-- <link href="{{URL::to('/')}}/dist/css/style.css" rel="stylesheet" type="text/css"> -->
<style>
	
.panel .panel-heading a {
    position: relative;
}
.inline-block {
    display: inline-block !important;
}
.mr-15 {
    margin-right: 15px !important;
}
.panel .panel-heading .pull-right i{
	font-size: 20px;
    color: #878787;
}
.panel .panel-heading a i {
    -webkit-transition: opacity 0.3s ease;
    -moz-transition: opacity 0.3s ease;
	transition: opacity 0.3s ease;
}
.jvectormap-tip{
	z-index:100;
}

</style>
@endpush
@section('content')

<div class="content wrapper theme-4-active pimary-color-red box-layout" style="padding-top:100px;">		
        <!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid pt-25">
				<div class="row">
					<div class="col-lg-8 col-xs-12">
                        <div class="panel panel-default card-view panel-refresh">
							<div class="refresh-container">
								<div class="la-anim-1"></div>
							</div>
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Audience location</h6>
								</div>
								<div class="pull-right">
									<a href="#" class="pull-left inline-block refresh mr-15">
										<i class="zmdi zmdi-replay"></i>
									</a>
									<a href="#" class="pull-left inline-block full-screen mr-15">
										<i class="zmdi zmdi-fullscreen"></i>
									</a>
									<div class="pull-left inline-block dropdown">
										<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="zmdi zmdi-more-vert"></i></a>
										<ul class="dropdown-menu bullet dropdown-menu-right"  role="menu">
											<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>option 1</a></li>
											<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-share" aria-hidden="true"></i>option 2</a></li>
											<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-trash" aria-hidden="true"></i>option 3</a></li>
										</ul>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
                                <div class="panel-body">
									<div id="world_map_marker_1" style="height: 385px"></div>
								</div>
							</div>
                        </div>
                    </div>
					<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">browser stats</h6>
								</div>
								<div class="pull-right">
									<a href="#" class="pull-left inline-block mr-15">
										<i class="zmdi zmdi-download"></i>
									</a>
									<a href="#" class="pull-left inline-block close-panel" data-effect="fadeOut">
										<i class="zmdi zmdi-close"></i>
									</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body row">
										<div class="table-wrap sm-data-box-2">
										<div class="table-responsive">
											<table  class="table  top-countries mb-0">
												<tbody>
													<tr>
														<td>
															<img src="dist/img/country/01-flag.png" alt="country">	
															<span class="country-name txt-dark pl-15">Aland</span>
														</td>
														<td class="text-right">
															<span class="badge badge-warning transparent-badge weight-500">57%</span>
														</td>
													</tr>
													<tr>
														<td>
															<img src="dist/img/country/02-flag.png" alt="country">	
															<span class="country-name txt-dark pl-15">Angola</span>
														</td>
														<td class="text-right">
															<span class="badge badge-danger transparent-badge weight-500">17%</span>
														</td>
													</tr>
													<tr>
														<td>
															<img src="dist/img/country/03-flag.png" alt="country">
															<span class="country-name txt-dark pl-15">Anguilla</span>
														</td>
														
														<td class="text-right">
															<span class="badge badge-info transparent-badge weight-500">27%</span>
														</td>
													</tr>
													<tr>
														<td>
															<img src="dist/img/country/04-flag.png" alt="country">
															<span class="country-name txt-dark pl-15">Bahrain</span>
														</td>
														<td class="text-right">
															<span class="badge badge-danger transparent-badge weight-500">17%</span>
														</td>
													</tr>
													<tr>
														<td>
															<img src="dist/img/country/05-flag.png"  alt="country">
															<span class="country-name txt-dark pl-15">Australia</span>
														</td>
														<td class="text-right">
															<span class="badge badge-primary transparent-badge weight-500">51%</span>
														</td>
													</tr>
													
													<tr>
														<td>
															<img src="dist/img/country/06-flag.png" alt="country">	
															<span class="country-name txt-dark pl-15">Austria</span>
														</td>
														
														<td class="text-right">
															<span class="badge badge-warning transparent-badge weight-500">10%</span>
														</td>
													</tr>
													<tr>
														<td>
															<img src="dist/img/country/07-flag.png" alt="country">
															<span class="country-name txt-dark pl-15">Belgium</span>
														</td>
														<td class="text-right">
															<span class="badge badge-success transparent-badge weight-500">27%</span>
														</td>
													</tr>
													<tr>
														<td class="txt-dark">
															Other	
														</td>
														
														<td class="text-right">
															<span class="badge badge-warning transparent-badge weight-500">10%</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
			
			</div>			
		</div>
        <!-- /Main Content -->

	</div>
	
    <!-- /#wrapper -->
	
	<!-- JavaScript -->
	   @include('sections.script_section2')
@endsection