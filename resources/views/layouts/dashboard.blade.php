<!DOCTYPE html>
<html lang="en">
<!-- START Head -->

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <!-- Enable responsiveness on mobile devices-->
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">

    <title>
        Dashboard | Status
    </title>

    
    <!--START Loader -->
    @stack('style2')
    <style>
        
        #initial-loader {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
            width: 100%;
            background: #212121;
            position: fixed;
            z-index: 10000;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            transition: opacity .2s ease-out
        }

        #initial-loader .initial-loader-top {
            display: flex;
            align-items: center;
            justify-content: space-between;
            width: 200px;
            border-bottom: 1px solid #2d2d2d;
            padding-bottom: 5px
        }

        #initial-loader .initial-loader-top>* {
            display: block;
            flex-shrink: 0;
            flex-grow: 0
        }

        #initial-loader .initial-loader-bottom {
            padding-top: 10px;
            color: #5C5C5C;
            font-family: -apple-system, "Helvetica Neue", Helvetica, "Segoe UI", Arial, sans-serif;
            font-size: 12px
        }

        @keyframes spin {
            100% {
                transform: rotate(360deg)
            }
        }

        #initial-loader .loader g {
            transform-origin: 50% 50%;
            animation: spin .5s linear infinite
        }

        body.loading {
            overflow: hidden !important
        }

        body.loaded #initial-loader {
            opacity: 0
        }
    </style>
    <!--END Loader-->

    <!-- SCSS Output -->
    <link rel="stylesheet" href="{{URL::to('/')}}/assets/stylesheets/app.min.e0bb64e7.css">

    <!-- START Favicon -->
    
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{URL::to('/')}}/assets/images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">    
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- END Favicon -->

    <!-- RSS -->
    <link rel="alternate" type="application/rss+xml" title="RSS" href="../atom.xml">
    	<!-- Favicon -->
	<script src="{{URL::to('/')}}/vendors/bower_components/jquery/dist/jquery.min.js"></script>    
    
    <!-- Google Analytics -->
    <script>       
        (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-83862026-1', 'auto');
        ga('send', 'pageview');
        
        var baseUrl = "{{URL::to('/')}}";
       
    </script>

    <!-- jekyll settings -->
    <script>
        var ASSET_PATH_BASE = '../';
    </script>
</head>
<!-- END Head -->


<body class="loading">
    
    <!-- Bower Libraries Scripts -->
    <script src="{{URL::to('/')}}/assets/vendor/js/lib.min.js"></script>

    <div class="main-wrap">
        <nav class="navigation">
            <!-- START Navbar -->
            <div class="navbar-inverse navbar navbar-fixed-top">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <a class="current navbar-brand" href="{{URL::to('/')}}/index.html">
                            <!-- <img alt="Spin Logo Inverted" class="h-20" src="{{URL::to('/')}}/assets/images/spin-logo-inverted-@2X.png"> -->
                        </a>
                        <button class="action-right-sidebar-toggle navbar-toggle collapsed" data-target="#navdbar" data-toggle="collapse" type="button">
                            <i class="fa fa-fw fa-align-right text-white"></i>
                        </button>
                        <button class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button">
                            <i class="fa fa-fw fa-user text-white"></i>
                        </button>
                        <button class="action-sidebar-open navbar-toggle collapsed" type="button">
                            <i class="fa fa-fw fa-bars text-white"></i>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="navbar">

                        <!-- START Search Mobile -->
                        <div class="form-group hidden-lg hidden-md hidden-sm">
                            <div class="input-group hidden-lg hidden-md">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fa fa-fw fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <!-- END Search Mobile -->

                        <!-- START Left Side Navbar -->
                        <ul class="nav navbar-nav navbar-left clearfix yamm">

                            <!-- START Switch Sidebar ON/OFF -->
                            <li id="sidebar-switch" class="hidden-xs">
                                <a class="action-toggle-sidebar-slim" data-placement="bottom" data-toggle="tooltip" href="#" title="Slim sidebar on/off">
                                    <i class="fa fa-lg fa-bars fa-fw"></i>
                                </a>
                            </li>
                            <!-- END Switch Sidebar ON/OFF -->

                            <!-- START Menu Only Visible on Navbar -->
                            <li id="top-menu-switch" class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu
                                    <i class="fa fa-fw fa-caret-down"></i>
                                </a>


                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">

                                                <!-- START Start, Widgets Navbar Menu -->
                                             
                                                <!-- END Start, Widgets Navbar Menu -->

                                                <!-- START Layouts, Sidebars Navbar Menu -->
                                             
                                                <!-- END Layouts, Sidebars Navbar Menu -->

                                                <!-- START Layouts, Sidebars Navbar Menu -->
                                                
                                                <!-- END Layouts, Sidebars Navbar Menu -->

                                                <!-- START Pages Navbar Menu -->
                                                <ul class="col-sm-2 list-unstyled">
                                                    <li>
                                                        <p class="text-muted small text-uppercase">
                                                            <strong> Pages</strong>
                                                        </p>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../pages/register.html">
                                                            <span class="nav-label">Register</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../pages/login.html">
                                                            <span class="nav-label">Login</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../pages/forgot-password.html">
                                                            <span class="nav-label">Forgot Password</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../pages/lock-screen.html">
                                                            <span class="nav-label">Lock Screen</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../pages/error-404.html">
                                                            <span class="nav-label">Error 404</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../pages/invoice.html">
                                                            <span class="nav-label">Invoice</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../pages/timeline.html">
                                                            <span class="nav-label">Timeline</span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <p class="text-muted small text-uppercase m-t-2">
                                                            <strong>Projects</strong>
                                                        </p>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/projects-list.html">
                                                            <span class="nav-label">Projects List</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/projects-grid.html">
                                                            <span class="nav-label">Projects Grid</span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <p class="text-muted small text-uppercase m-t-2">
                                                            <strong> Tasks</strong>
                                                        </p>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/tasks-list.html">
                                                            <span class="nav-label">Tasks List</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/tasks-grid.html">
                                                            <span class="nav-label">Tasks Grid</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/tasks-details.html">
                                                            <span class="nav-label">Tasks Details</span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <p class="text-muted small text-uppercase m-t-2">
                                                            <strong>Files Manager</strong>
                                                        </p>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/files-list.html">
                                                            <span class="nav-label">Files List</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/files-grid.html">
                                                            <span class="nav-label">Files Grid</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <p class="text-muted small text-uppercase m-t-2">
                                                            <strong> Search Results</strong>
                                                        </p>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/search-results.html">
                                                            <span class="nav-label">Search Results</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/images-results.html">
                                                            <span class="nav-label">Images Results</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/videos-results.html">
                                                            <span class="nav-label">Videos Results</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/users-results.html">
                                                            <span class="nav-label">Users Results</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <!-- END Pages Navbar Menu -->

                                                <!-- START Pages Navbar Menu -->
                                                <ul class="col-sm-2 list-unstyled">
                                                    <li>
                                                        <p class="text-muted small text-uppercase">
                                                            <strong> Users</strong>
                                                        </p>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/users-list.html">
                                                            <span class="nav-label">Users List</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/users-grid.html">
                                                            <span class="nav-label">Users Grid</span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <p class="text-muted small text-uppercase m-t-2">
                                                            <strong>Mailbox</strong>
                                                        </p>
                                                    </li>

                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/inbox.html">
                                                            <span class="nav-label">Inbox</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/email-details.html">
                                                            <span class="nav-label">Email Details</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/new-email.html">
                                                            <span class="nav-label">New Email</span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <p class="text-muted small text-uppercase m-t-2">
                                                            <strong> Profile User</strong>
                                                        </p>
                                                    </li>

                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/profile-details.html">
                                                            <span class="nav-label">Profile Details</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/profile-edit.html">
                                                            <span class="nav-label">Profile Edit</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/account-edit.html">
                                                            <span class="nav-label">Account Edit</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/billing-edit.html">
                                                            <span class="nav-label">Billing Edit</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/settings-edit.html">
                                                            <span class="nav-label">Settings Edit</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../apps/sessions-edit.html">
                                                            <span class="nav-label">Sessions Edit</span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <p class="text-muted small text-uppercase m-t-2">
                                                            <strong> Icons</strong>
                                                        </p>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../icon/fonts-awesome.html">
                                                            <span class="nav-label">Fonts Awesome</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../icon/glyphicons.html">
                                                            <span class="nav-label">Glyphicons</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <p class="text-muted small text-uppercase m-t-2">
                                                            <strong>Versions</strong>
                                                        </p>
                                                    </li>`
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../versions/jquery.html">
                                                            <span class="nav-label">JQuery</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../versions/react.html">
                                                            <span class="nav-label">React</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../versions/react.html">
                                                            <span class="nav-label">Photoshop .PSD</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <!-- END Pages Navbar Menu -->

                                                <!-- START Pages Navbar Menu -->
                                                <ul class="col-sm-2 list-unstyled">
                                                    <li>
                                                        <p class="text-muted small text-uppercase">
                                                            <strong>Forms</strong>
                                                        </p>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../forms/forms.html">
                                                            <span class="nav-label">Forms</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../forms/forms-layouts.html">
                                                            <span class="nav-label">Forms Layouts</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../forms/date-range-picker.html">
                                                            <span class="nav-label">Date Range Picker</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../forms/forms-extended.html">
                                                            <span class="nav-label">Forms Extended</span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <p class="text-muted small text-uppercase m-t-2">
                                                            <strong>Panels</strong>
                                                        </p>
                                                    </li>
                                                    <li class="m-l-1">
                                                        <a class="text-gray-lighter" href="../panels/panels.html">
                                                            <span class="nav-label">Panels</span>
                                                        </a>
                                                    </li>

                                                </ul>
                                                <!-- END Pages Navbar Menu -->

                                            </div>
                                        </div>
                                    </li>
                                </ul>

                            </li>
                            <!-- END Menu Only Visible on Navbar -->


                            <li class="spin-search-box clearfix hidden-xs">
                                <a href="#" class="pull-left">
                                    <i class="fa fa-search fa-lg icon-inactive"></i>
                                    <i class="fa fa-close fa-lg icon-active"></i>
                                </a>
                                <div class="input-group input-group-sm pull-left p-10">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </li>
                        </ul>
                        <!-- START Left Side Navbar -->

                        <!-- START Right Side Navbar -->
                        <ul class="nav navbar-nav navbar-right">

                            <li role="separator" class="divider hidden-lg hidden-md hidden-sm"></li>
                            <li class="dropdown-header hidden-lg hidden-md hidden-sm text-gray-lighter text-uppercase">
                                <strong>Your Profile</strong>
                            </li>

                            <!-- START Notification -->
                            <li class="dropdown">

                                <!-- START Icon Notification with Badge (10)-->
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">
                                    <i class="fa fa-lg fa-fw fa-bell hidden-xs"></i>
                                    <span class="hidden-sm hidden-md hidden-lg">
                                        Notifications
                                        <span class="badge badge-primary m-l-1">10</span>
                                    </span>
                                    <span class="label label-primary label-pill label-with-icon hidden-xs">10</span>
                                    <span class="fa fa-fw fa-angle-down hidden-lg hidden-md hidden-sm"></span>
                                </a>
                                <!-- END Icon Notification with Badge (10)-->

                                <!-- START Notification Dropdown Menu -->
                                <ul class="dropdown-menu dropdown-menu-right p-t-0 b-t-0 p-b-0 b-b-0 b-a-0">
                                    <li>
                                        <div class="yamm-content p-t-0 p-r-0 p-l-0 p-b-0">
                                            <ul class="list-group m-b-0 b-b-0">
                                                <li class="list-group-item b-r-0 b-l-0 b-r-0 b-t-r-0 b-t-l-0 b-b-2 w-350">
                                                    <small class="text-uppercase">
                                                        <strong>Notifications</strong>
                                                    </small>
                                                    <a role="button" href="../apps/settings-edit.html" class="btn m-t-0 btn-xs btn-default pull-right">
                                                        <i class="fa fa-fw fa-gear"></i>
                                                    </a>
                                                </li>

                                                <!-- START Scroll Inside Panel -->
                                                <li class="list-group-item b-a-0 p-x-0 p-y-0 b-t-0">
                                                    <div class="scroll-300 custom-scrollbar">
                                                        <a href="../pages/timeline.html" class="list-group-item b-r-0 b-t-0 b-l-0">
                                                            <div class="media">
                                                                <div class="media-left">
                                                                    <span class="fa-stack fa-lg">
                                                                        <i class="fa fa-circle-thin fa-stack-2x text-danger"></i>
                                                                        <i class="fa fa-close fa-stack-1x fa-fw text-danger"></i>
                                                                    </span>
                                                                </div>
                                                                <div class="media-body">
                                                                    <h5 class="m-t-0">
                                                                        <span>I&apos;ll reboot the bluetooth AI bus, that should
                                                                            interface the COM microchip!</span>
                                                                    </h5>
                                                                    <p class="text-nowrap small m-b-0">
                                                                        <span>27-Sep-2013, 08:39</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <a href="../pages/timeline.html" class="list-group-item b-r-0 b-l-0">
                                                            <div class="media">
                                                                <div class="media-left">
                                                                    <span class="fa-stack fa-lg">
                                                                        <i class="fa fa-circle-thin fa-stack-2x text-primary"></i>
                                                                        <i class="fa fa-info fa-stack-1x text-primary"></i>
                                                                    </span>
                                                                </div>
                                                                <div class="media-body">
                                                                    <h5 class="m-t-0">
                                                                        <span>Try to bypass the IB alarm, maybe it will index the
                                                                            solid state application!</span>
                                                                    </h5>
                                                                    <p class="text-nowrap small m-b-0">
                                                                        <span>30-May-2016, 07:42</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <a href="../pages/timeline.html" class="list-group-item b-r-0 b-l-0">
                                                            <div class="media">
                                                                <div class="media-left">
                                                                    <span class="fa-stack fa-lg">
                                                                        <i class="fa fa-circle-thin fa-stack-2x text-success"></i>
                                                                        <i class="fa fa-check fa-stack-1x text-success"></i>
                                                                    </span>
                                                                </div>
                                                                <div class="media-body">
                                                                    <h5 class="m-t-0">
                                                                        <span>We need to compress the wireless SQL protocol!</span>
                                                                    </h5>
                                                                    <p class="text-nowrap small m-b-0">
                                                                        <span>18-Sep-2013, 04:33</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <a href="../pages/timeline.html" class="list-group-item b-r-0 b-l-0">
                                                            <div class="media">
                                                                <div class="media-left">
                                                                    <span class="fa-stack fa-lg">
                                                                        <i class="fa fa-circle-thin fa-stack-2x text-warning"></i>
                                                                        <i class="fa fa-exclamation fa-stack-1x fa-fw text-warning"></i>
                                                                    </span>
                                                                </div>
                                                                <div class="media-body">
                                                                    <h5 class="m-t-0">
                                                                        <span>programming the pixel won&apos;t do anything, we
                                                                            need to back up the solid state SSL panel!</span>
                                                                    </h5>
                                                                    <p class="text-nowrap small m-b-0">
                                                                        <span>11-Jan-2013, 09:44</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </li>

                                                <!-- END Scroll Inside Panel -->
                                                <li class="list-group-item b-a-0 p-x-0 p-y-0 r-a-0 b-b-0">
                                                    <a class="list-group-item text-center b-r-0 b-b-0 b-l-0 b-r-b-r-0 b-r-b-l-0" href="../pages/timeline.html">
                                                        See All Notifications
                                                        <i class="fa fa-angle-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                    </li>
                                </ul>
                                <!-- END Notification Dropdown Menu -->

                            </li>
                            <!-- END Notification -->

                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">
                                    <i class="fa fa-lg fa-fw fa-envelope hidden-xs"></i>
                                    <span class="hidden-sm hidden-md hidden-lg">Messages
                                        <span class="badge badge-info m-l-1">3</span>
                                    </span>
                                    <span class="label label-info label-pill label-with-icon hidden-xs">3</span>
                                    <span class="fa fa-fw fa-angle-down hidden-lg hidden-md hidden-sm"></span>
                                </a>

                                <!-- START Messages Dropdown Menu -->
                                <ul class="dropdown-menu dropdown-menu-right p-t-0 b-t-0 p-b-0 b-b-0 b-a-0">
                                    <li>
                                        <div class="yamm-content p-t-0 p-r-0 p-l-0 p-b-0">
                                            <ul class="list-group m-b-0">
                                                <li class="list-group-item b-r-0 b-l-0 b-r-0 b-t-r-0 b-t-l-0 b-b-2 w-350">
                                                    <small class="text-uppercase">
                                                        <strong>Messages</strong>
                                                    </small>
                                                    <a role="button" href="../apps/new-email.html" class="btn m-t-0 btn-xs btn-default pull-right">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </li>

                                                <!-- START Scroll Inside Panel -->
                                                <li class="list-group-item b-a-0 p-x-0 p-y-0 b-t-0">
                                                    <div class="scroll-200 custom-scrollbar">

                                                        <a href="../apps/email-details.html" class="list-group-item b-r-0 b-t-0 b-l-0">
                                                            <div class="media">
                                                                <div class="media-left media-middle">
                                                                    <div class="avatar">
                                                                        <img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/andysolomon/128.jpg" alt="Avatar">
                                                                        <i class="avatar-status avatar-status-bottom bg-danger b-gray-darker"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="media-body media-auto">
                                                                    <h5 class="m-b-0 m-t-0">
                                                                        <span>Jovani Brown</span>
                                                                        <small>
                                                                            <span>04:59</span>
                                                                        </small>
                                                                    </h5>
                                                                    <p class="m-t-0 m-b-0">
                                                                        <span>Cupiditate molestiae et ad qui labore id voluptas
                                                                            dicta possimus.</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>

                                                        <a href="../apps/email-details.html" class="list-group-item b-r-0 b-l-0">
                                                            <div class="media">
                                                                <div class="media-left media-middle">
                                                                    <div class="avatar">
                                                                        <img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/rweve/128.jpg" alt="Avatar">
                                                                        <i class="avatar-status avatar-status-bottom bg-warning b-gray-darker"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="media-body media-auto">
                                                                    <h5 class="m-b-0 m-t-0">
                                                                        <span>Davin Grimes</span>
                                                                        <small>
                                                                            <span>09:10</span>
                                                                        </small>
                                                                    </h5>
                                                                    <p class="m-t-0 m-b-0">
                                                                        <span>Dolorem ut qui ad.</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>

                                                        <a href="../apps/email-details.html" class="list-group-item b-r-0 b-l-0">
                                                            <div class="media">
                                                                <div class="media-left media-middle">
                                                                    <div class="avatar">
                                                                        <img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/swaplord/128.jpg" alt="Avatar">
                                                                        <i class="avatar-status avatar-status-bottom bg-success b-gray-darker"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="media-body media-auto">
                                                                    <h5 class="m-b-0 m-t-0">
                                                                        <span>Ward Carter</span>
                                                                        <small>
                                                                            <span>04:31</span>
                                                                        </small>
                                                                    </h5>
                                                                    <p class="m-t-0 m-b-0">
                                                                        <span>Nihil sint ab.</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>

                                                        <a href="../apps/email-details.html" class="list-group-item b-r-0 b-l-0">
                                                            <div class="media">
                                                                <div class="media-left media-middle">
                                                                    <div class="avatar">
                                                                        <img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/bcrad/128.jpg" alt="Avatar">
                                                                        <i class="avatar-status avatar-status-bottom bg-danger b-gray-darker"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="media-body media-auto">
                                                                    <h5 class="m-b-0 m-t-0">
                                                                        <span>Lera Carroll</span>
                                                                        <small>
                                                                            <span>02:11</span>
                                                                        </small>
                                                                    </h5>
                                                                    <p class="m-t-0 m-b-0">
                                                                        <span>Molestiae fugiat earum qui exercitationem possimus.</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>

                                                        <a href="../apps/email-details.html" class="list-group-item b-r-0 b-l-0">
                                                            <div class="media">
                                                                <div class="media-left media-middle">
                                                                    <div class="avatar">
                                                                        <img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/jennyyo/128.jpg" alt="Avatar">
                                                                        <i class="avatar-status avatar-status-bottom bg-warning b-gray-darker"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="media-body media-auto">
                                                                    <h5 class="m-b-0 m-t-0">
                                                                        <span>Celine Bins</span>
                                                                        <small>
                                                                            <span>08:43</span>
                                                                        </small>
                                                                    </h5>
                                                                    <p class="m-t-0 m-b-0">
                                                                        <span>Ut totam placeat dicta.</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>

                                                        <a href="../apps/email-details.html" class="list-group-item b-r-0 b-l-0">
                                                            <div class="media">
                                                                <div class="media-left media-middle">
                                                                    <div class="avatar">
                                                                        <img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/fabbianz/128.jpg" alt="Avatar">
                                                                        <i class="avatar-status avatar-status-bottom bg-success b-gray-darker"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="media-body media-auto">
                                                                    <h5 class="m-b-0 m-t-0">
                                                                        <span>Willy Baumbach</span>
                                                                        <small>
                                                                            <span>06:30</span>
                                                                        </small>
                                                                    </h5>
                                                                    <p class="m-t-0 m-b-0">
                                                                        <span>Nam quia corrupti tempora minus aut facilis ipsum.</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>

                                                    </div>
                                                </li>
                                                <!-- END Scroll Inside Panel -->

                                                <li class="list-group-item b-a-0 p-x-0 b-b-0 p-y-0 r-a-0">
                                                    <a class="list-group-item text-center b-b-0 b-r-0 b-l-0 b-r-b-r-0 b-r-b-l-0" href="../apps/inbox.html">
                                                        See All Messages
                                                        <i class="fa fa-angle-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                                <!-- END Messages Dropdown Menu -->

                            </li>

                            <li class="dropdown">
                                <a class="dropdown-toggle user-dropdown" data-toggle="dropdown" href="#" role="button">
                                    <span class="m-r-1">Kraig Schuster</span>
                                    <div class="avatar avatar-image avatar-sm avatar-inline">
                                        <img alt="User" src="https://s3.amazonaws.com/uifaces/faces/twitter/collegeman/128.jpg">
                                    </div>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-header text-gray-lighter">
                                        <strong class="text-uppercase">Account</strong>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="../apps/profile-details.html">Your Profile</a>
                                    </li>
                                    <li>
                                        <a href="../apps/profile-edit.html">Settings</a>
                                    </li>
                                    <li>
                                        <a href="../apps/faq.html">Faq</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="{{ route('logout')}}">Sign Out</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="hidden-xs">
                                <a class="action-right-sidebar-toggle" title="Right sidebar on/off">
                                    <i class="fa fa-lg fa-align-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- END Right Side Navbar -->
                    </div>


                </div>
            </div>
            <!-- END Navbar -->

            <!-- START Sidebars -->
            <aside class="navbar-default sidebar">
                <div class="sidebar-overlay-head">
                    <img src="../assets/images/spin-logo-inverted.png" alt="Logo" width="90">
                    <a href="#" class="sidebar-switch action-sidebar-close">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="sidebar-logo">
                    <img class="logo-default" src="../assets/images/spin-logo-big-inverse-@2X.png" alt="Logo" width="53">
                    <img class="logo-slim" src="../assets/images/spin-logo-slim.png" alt="Logo" height="13">
                </div>

                <div class="sidebar-content">
                    <div class="p-y-3 avatar-container">
                        <img src="../assets/images/avatars/spin-avatar-woman.png" width="50" alt="Avatar" class="spin-avatar img-circle">
                        <div class="text-center">
                            <h6 class="m-b-0">Michelle Baez</h6>
                            <small class="text-muted">Help Desk</small>
                            <p class="m-t-1 m-b-2">
                                <span id="sidebar-avatar-chart">5,3,2,-1,-3,-2,2,3,5,2</span>
                            </p>
                        </div>
                    </div>
                    <div class="sidebar-default-visible text-muted small text-uppercase sidebar-section p-y-2">
                        <strong>Navigation</strong>
                    </div>

                    <!-- START Tree Sidebar Common -->
                    <ul class="side-menu">

                        <li class="Dashboards">
                            <a href="#" title="Dashboards">
                                <i class="fa fa-home fa-lg fa-fw"></i>
                                <span class="nav-label">Start</span>
                                <i class="fa arrow"></i>
                            </a>
                            <ul>
                                <li class="">
                                    <a href="{{URL::to('/home')}}">
                                        <span class="nav-label">Dashboard</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="{{URL::to('/browser and map')}}">
                                        <span class="nav-label">Browser and Maps</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="{{URL::to('/userlist')}}">
                                        <span class="nav-label">User Lists</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" title="Dashboards">
                                <i class="fa fa-home fa-lg fa-fw"></i>
                                <span class="nav-label">Managers</span>
                                <i class="fa arrow"></i>
                            </a>
                            <ul>
                                <li class="">
                                    <a href="{{URL::to('/login-manager')}}">
                                        <span class="nav-label">Login-Manager</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="{{URL::to('/track-manager')}}">
                                        <span class="nav-label">Track-Manager</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="{{URL::to('/playlist-manager')}}">
                                        <span class="nav-label">Playlist-Manager</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- END Tree Sidebar Common  -->


                    <div class="sidebar-default-visible">
                        <hr>
                    </div>

                </div>
            </aside>
            <!-- END Sidebars -->


        @yield('content');
        </nav>


        <!-- START Footer -->
        <footer>
            <div class="container-fluid">
                <p class="text-gray-dark">
                    <strong class="m-r-1">SPIN Dashboard </strong>
                    <span class="text-gray-dark">&#xA9; 2009 - 2016. Made by
                        <i class="fa fa-fw fa-heart text-danger"></i> New York, US</span>
                </p>
            </div>
        </footer>
        <!-- END Footer -->

    </div>
    
    @stack('footer_script')    
    
</body>

            <!-- START Modal: Edit Profile -->
            <div class="modal fade modal-edit-profile" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&#xD7;</span>
                            </button>
                            <h4 class="modal-title">Edit Profile
                                
                            </h4>
                        </div>

                        <div class="modal-body">

                            <form class="form-horizontal">  
                                <div class="form-group">
                                    <label for="profile-first-name" class="col-sm-3 control-label">
                                        <span class="text-danger">*</span> Email Address</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="email" placeholder="Email...">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="profile-last-name" class="col-sm-3 control-label">Password</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="password" placeholder="Password...">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="profile-last-name" class="col-sm-3 control-label">Role</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="profile-role-type">
                                            <option>Admin</option>
                                            <option>Regular</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
             
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id = "editbtn" onclick="editUser()">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Modal: Edit Profile -->
            <div class="modal fade modal-edit-profile-one" id="modal-add-user" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&#xD7;</span>
                            </button>
                            <h4 class="modal-title">Add User
                                
                            </h4>
                        </div>

                        <div class="modal-body">

                            <form class="form-horizontal">   
                                <div class="form-group">
                                    <label for="profile-first-name" class="col-sm-3 control-label">
                                        <span class="text-danger">*</span> Email Address</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="emailadd" placeholder="Email...">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="profile-last-name" class="col-sm-3 control-label">Password</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="passwordadd" placeholder="Password...">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="profile-last-name" class="col-sm-3 control-label">Role</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="roletype">
                                            <option>Admin</option>
                                            <option>Regular</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
             
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" id = "addbtn">Add</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- START Modal Large: Select Plan -->
            <div class="modal fade modal-select-plan" id="modal-select-plan" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&#xD7;</span>
                            </button>
                            <h4 class="modal-title">Change Your Plan</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">

                                <!-- START Select Plan -->
                                <div class="col-md-3 m-b-3">
                                    <div class="text-center m-t-3">
                                        <span class="label label-info label-outline text-uppercase">Basic</span>
                                        <h1 class="display-4 m-t-1 m-b-0">$23.00</h1>
                                        <p>/ month</p>
                                        <div class="hr-text m-t-2 hr-text-center">
                                            <h6 class="text-white bg-rangoon-i">
                                                <strong>Description</strong>
                                            </h6>
                                        </div>
                                        Very good to start
                                        <br>your business
                                    </div>
                                    <div class="hr-text m-t-2 hr-text-center">
                                        <h6 class="text-white bg-rangoon-i">
                                            <strong>Capabilites</strong>
                                        </h6>
                                    </div>
                                    <!-- START Table: System -->
                                    <table class="table v-a-m">
                                        <tbody>
                                            <tr class="v-a-m b-t-0">
                                                <td class="b-t-0"> Android / iOS </td>
                                                <td class="text-right v-a-m text-white b-t-0">Yes</td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Admin Web Access </td>
                                                <td class="text-right v-a-m text-white">
                                                    <span>37186</span>
                                                </td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Appoinments </td>
                                                <td class="text-right v-a-m text-white">Yes</td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Import/Export Data </td>
                                                <td class="text-right v-a-m text-white">Yes</td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Data Storage</td>
                                                <td class="text-right v-a-m text-white">1GB</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- END Table: System -->
                                    <button type="button" class="btn btn-default btn-lg btn-block" data-dismiss="modal">Upgrade</button>
                                </div>
                                <!-- END Select Plan -->


                                <!-- START Select Plan -->
                                <div class="col-md-3 m-b-3">
                                    <div class="text-center m-t-3">
                                        <span class="label label-primary label-outline text-uppercase">Premium</span>
                                        <h1 class="display-4 m-t-1 m-b-0">$48.90</h1>
                                        <p>/ month</p>
                                        <div class="hr-text m-t-2 hr-text-center">
                                            <h6 class="text-white bg-rangoon-i">
                                                <strong>Description</strong>
                                            </h6>
                                        </div>
                                        Our most popular
                                        <br>package
                                    </div>
                                    <div class="hr-text m-t-2 hr-text-center">
                                        <h6 class="text-white bg-rangoon-i">
                                            <strong>Capabilites</strong>
                                        </h6>
                                    </div>
                                    <!-- START Table: System -->
                                    <table class="table v-a-m">
                                        <tbody>
                                            <tr class="v-a-m b-t-0">
                                                <td class="b-t-0"> Android / iOS </td>
                                                <td class="text-right v-a-m text-white b-t-0">Yes</td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Admin Web Access </td>
                                                <td class="text-right v-a-m text-white">
                                                    <span>28786</span>
                                                </td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Appoinments </td>
                                                <td class="text-right v-a-m text-white">Yes</td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Import/Export Data </td>
                                                <td class="text-right v-a-m text-white">Yes</td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Data Storage</td>
                                                <td class="text-right v-a-m text-white">2GB</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- END Table: System -->
                                    <button type="button" class="btn btn-primary btn-lg btn-block" disabled="disabled">
                                        <i class="fa fa-fw fa-check"></i> Selected</button>
                                </div>
                                <!-- END Select Plan -->


                                <!-- START Select Plan -->
                                <div class="col-md-3 m-b-3">
                                    <div class="text-center m-t-3">
                                        <span class="label label-warning label-outline text-uppercase">Pro</span>
                                        <h1 class="display-4 m-t-1 m-b-0">$79.99</h1>
                                        <p>/ month</p>
                                        <div class="hr-text m-t-2 hr-text-center">
                                            <h6 class="text-white bg-rangoon-i">
                                                <strong>Description</strong>
                                            </h6>
                                        </div>
                                        You have a lot of customers
                                        <br> take control of them.
                                    </div>
                                    <div class="hr-text m-t-2 hr-text-center">
                                        <h6 class="text-white bg-rangoon-i">
                                            <strong>Capabilites</strong>
                                        </h6>
                                    </div>
                                    <!-- START Table: System -->
                                    <table class="table v-a-m">
                                        <tbody>
                                            <tr class="v-a-m b-t-0">
                                                <td class="b-t-0"> Android / iOS </td>
                                                <td class="text-right v-a-m text-white b-t-0">Yes</td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Admin Web Access </td>
                                                <td class="text-right v-a-m text-white">
                                                    <span>65603</span>
                                                </td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Appoinments </td>
                                                <td class="text-right v-a-m text-white">Yes</td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Import/Export Data </td>
                                                <td class="text-right v-a-m text-white">Yes</td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Data Storage</td>
                                                <td class="text-right v-a-m text-white">3GB</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- END Table: System -->
                                    <button type="button" class="btn btn-default btn-lg btn-block" data-dismiss="modal">Upgrade</button>
                                </div>
                                <!-- END Select Plan -->


                                <!-- START Select Plan -->
                                <div class="col-md-3 m-b-3">
                                    <div class="text-center m-t-3">
                                        <span class="label label-danger label-outline text-uppercase">Advanced</span>
                                        <h1 class="display-4 m-t-1 m-b-0">$123.00</h1>
                                        <p>/ month</p>
                                        <div class="hr-text m-t-2 hr-text-center">
                                            <h6 class="text-white bg-rangoon-i">
                                                <strong>Description</strong>
                                            </h6>
                                        </div>
                                        For the most advanced
                                        <br>users and teams
                                    </div>
                                    <div class="hr-text m-t-2 hr-text-center">
                                        <h6 class="text-white bg-rangoon-i">
                                            <strong>Capabilites</strong>
                                        </h6>
                                    </div>
                                    <!-- START Table: System -->
                                    <table class="table v-a-m">
                                        <tbody>
                                            <tr class="v-a-m b-t-0">
                                                <td class="b-t-0"> Android / iOS </td>
                                                <td class="text-right v-a-m text-white b-t-0">Yes</td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Admin Web Access </td>
                                                <td class="text-right v-a-m text-white">
                                                    <span>42006</span>
                                                </td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Appoinments </td>
                                                <td class="text-right v-a-m text-white">Yes</td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Import/Export Data </td>
                                                <td class="text-right v-a-m text-white">Yes</td>
                                            </tr>
                                            <tr class="v-a-m">
                                                <td> Data Storage</td>
                                                <td class="text-right v-a-m text-white">4GB</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- END Table: System -->
                                    <button type="button" class="btn btn-default btn-lg btn-block" data-dismiss="modal">Upgrade</button>
                                </div>
                                <!-- END Select Plan -->
                            </div>



                        </div>

                        <!-- START Modal Footer -->
                        <div class="modal-footer">

                            <span class="label label-gray-lighter label-outline text-uppercase">Free</span>
                            <span class="m-l-1">
                                <strong>Lorem ipsum dolor sit amet</strong>
                            </span>, consectetur adipisicing
                            <a href="#" data-dismiss="modal">Downgrade Now</a>

                        </div>
                        <!-- END Modal Footer -->

                    </div>
                </div>
            </div>
            <!-- END Modal Large: Select Plan -->
            <!-- START Modal: Profile -->
            <div class="modal fade modal-profile" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&#xD7;</span>
                            </button>
                            <h4 class="modal-title">Profile Details</h4>
                        </div>

                        <div class="modal-body">
                            <!-- START Avatar with Name -->
                            <div class="media m-l-1">
                                <div class="media-left">
                                    <div class="center-block">
                                        <div class="avatar avatar-image avatar-lg center-block">
                                            <img class="img-circle center-block m-t-1 m-b-2" src="https://s3.amazonaws.com/uifaces/faces/twitter/danpliego/128.jpg" alt="Avatar">
                                            <i class="avatar-status avatar-status-bottom bg-danger b-brand-gray-darker"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h4>
                                        <span>Lavinia Price</span>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Add to Favorites">
                                            <i class="fa fa-fw fa-star-o text-muted fa-lg"></i>
                                        </a>
                                    </h4>
                                    <p class="m-t-0">
                                        <span>Dynamic Brand Analyst</span>
                                    </p>

                                    <div class="btn-toolbar" role="toolbar">
                                        <div class="btn-group" role="group">
                                            <!-- Standard button -->
                                            <button type="button" class="btn btn-primary">
                                                <i class="fa fa-fw fa-envelope-o m-r-1"></i>Send Email</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <!-- Standard button -->
                                            <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Edit">
                                                <i class="fa fa-fw fa-pencil"></i>
                                            </button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" class="btn btn-default" data-placement="top" title="Delete" data-toggle="modal" data-target=".modal-alert-danger">
                                                <i class="fa fa-fw fa-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Avatar with Name -->

                            <div class="hr-text hr-text-left">
                                <h6 class="text-white bg-rangoon-i">
                                    <strong>Profile</strong>
                                </h6>
                            </div>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta sapiente earum, necessitatibus commodi eius pariatur repudiandae
                            cum sunt officiis ex!
                            <div class="hr-text hr-text-left m-t-2">
                                <h6 class="text-white bg-rangoon-i">
                                    <strong>Labels</strong>
                                </h6>
                            </div>
                            <span class="label label-gray label-outline">
                                <span>Automotive</span>
                            </span>
                            <span class="label label-gray label-outline">
                                <span>Garden</span>
                            </span>
                            <span class="label label-gray label-outline">
                                <span>Kids</span>
                            </span>

                            <!-- Tabs Buttons -->
                            <ul class="nav nav-tabs m-t-3 tab-color-rangoon">
                                <li role="presentation" class="active">
                                    <a data-toggle="tab" href="#tab-detail-contact-2" role="tab">Detail Contact</a>
                                </li>

                                <li role="presentation">
                                    <a data-toggle="tab" href="#tab-activity" role="tab">Activity</a>
                                </li>
                            </ul>

                            <!-- START Tabs Content -->
                            <div class="tab-content">

                                <!-- START Tab: Detail Contact -->
                                <div class="tab-pane fade in active p-r-1 tab-color-rangoon" id="tab-detail-contact-2" role="tabpanel">
                                    <div class="hr-text hr-text-left m-t-1">
                                        <h6 class="text-white bg-rangoon-i">
                                            <strong>Contacts</strong>
                                        </h6>
                                    </div>
                                    <dl class="dl-horizontal text-left">
                                        <dt class="text-left">Phone</dt>
                                        <dd class="text-left text-white">
                                            <span>567-241-9688</span>
                                        </dd>
                                        <dt class="text-left">Mobile</dt>
                                        <dd class="text-left text-white">
                                            <span>368-120-2978</span>
                                        </dd>
                                        <dt class="text-left">Fax</dt>
                                        <dd class="text-left text-white">
                                            <span>097-873-9756</span>
                                        </dd>
                                        <dt class="text-left">Email</dt>
                                        <dd class="text-left text-white">
                                            <a href="#">
                                                <span>Haleigh97@hotmail.com</span>
                                            </a>
                                        </dd>
                                        <dt class="text-left">Skype</dt>
                                        <dd class="text-left text-white">
                                            <a href="#">
                                                <span>Allison.Daugherty73</span>
                                            </a>
                                        </dd>
                                    </dl>
                                    <div class="hr-text hr-text-left m-t-1">
                                        <h6 class="text-white bg-rangoon-i">
                                            <strong>Social</strong>
                                        </h6>
                                    </div>
                                    <dl class="dl-horizontal text-left">
                                        <dt class="text-left">
                                            <i class="fa fa-fw fa-linkedin-square"></i> Linkedin</dt>
                                        <dd class="text-left text-white">
                                            <a href="#">http://lnkd.in/
                                                <span>Karlee_Schultz</span>
                                            </a>
                                        </dd>
                                        <dt class="text-left">
                                            <i class="fa fa-fw fa-facebook-square"></i> Facebook</dt>
                                        <dd class="text-left text-white">
                                            <a href="#">http://fb.com/
                                                <span>Verner_Ritchie43</span>
                                            </a>
                                        </dd>
                                        <dt class="text-left">
                                            <i class="fa fa-fw fa-twitter-square"></i> Twitter</dt>
                                        <dd class="text-left text-white">
                                            <a href="#">http://twitter.com/
                                                <span>Kameron21</span>
                                            </a>
                                        </dd>
                                    </dl>
                                    <div class="hr-text hr-text-left m-t-1">
                                        <h6 class="text-white bg-rangoon-i">
                                            <strong>Address</strong>
                                        </h6>
                                    </div>
                                    <dl class="dl-horizontal text-left">
                                        <dt class="text-left">Address</dt>
                                        <dd class="text-left text-white">
                                            <span>932 Blanca Divide</span>
                                        </dd>
                                        <dt class="text-left">City</dt>
                                        <dd class="text-left text-white">
                                            <span>Reichelshire</span>
                                        </dd>
                                        <dt class="text-left">State</dt>
                                        <dd class="text-left text-white">
                                            <span>Nevada</span>
                                        </dd>
                                        <dt class="text-left">ZIP Code</dt>
                                        <dd class="text-left text-white">
                                            <span>37757-4051</span>
                                        </dd>
                                    </dl>
                                </div>
                                <!-- END Tab: Detail Contact -->

                                <!-- START Tab: Activity -->
                                <div class="tab-pane fade p-r-1" id="tab-activity" role="tabpanel">
                                    <!-- START Timeline - Post -->
                                    <div class="timeline timeline-datetime">
                                        <!-- Timeline - Badge Date -->
                                        <div class="timeline-date">
                                            <span class="badge">Today</span>
                                        </div>
                                        <div class="timeline-item p-r-1">
                                            <!-- Timeline - Mini Icon -->
                                            <div class="timeline-icon">
                                                <i class="fa fa-circle-o text-danger"></i>
                                            </div>
                                            <div class="timeline-item-inner">
                                                <!-- Timeline - Date -->
                                                <span class="timeline-item-time">11:09 am</span>
                                                <!-- Timeline - Icon  -->
                                                <div class="timeline-item-head clearfix">
                                                    <div class="pull-left m-r-1">
                                                        <span class="fa-stack fa-lg">
                                                            <i class="fa fa-circle fa-stack-2x text-danger"></i>
                                                            <i class="fa fa-exclamation fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                    </div>
                                                    <!-- Timeline - Header & Subtitle -->
                                                    <div class="user-detail">
                                                        <h5>
                                                            <span>Re-contextualized clear-thinking ability</span>
                                                        </h5>
                                                        <p class="m-t-0 m-b-0 small text-uppercase">
                                                            <strong>
                                                                <span>Keeling - Greenfelder</span>
                                                            </strong>
                                                        </p>
                                                    </div>
                                                </div>
                                                <!-- Timeline - START Content -->
                                                <div class="timeline-item-content m-b-0">
                                                    <p class="m-b-1">
                                                        <span>Quidem est reiciendis. Repellat repellat nulla rerum esse. Ducimus
                                                            quo nam molestias. Amet voluptates et dolores est earum similique
                                                            laboriosam. Fuga eum aspernatur beatae at id deserunt sit nesciunt
                                                            molestiae.</span>
                                                    </p>
                                                </div>
                                                <!-- Timeline - END Content -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Timeline - Post -->

                                    <!-- START Timeline - Post -->
                                    <div class="timeline timeline-datetime">
                                        <div class="timeline-item p-r-1">
                                            <!-- Timeline - Mini Icon -->
                                            <div class="timeline-icon">
                                                <i class="fa fa-circle-o text-info"></i>
                                            </div>
                                            <div class="timeline-item-inner">
                                                <!-- Timeline - Date -->
                                                <span class="timeline-item-time">0:12 am</span>
                                                <!-- Timeline - Icon  -->
                                                <div class="timeline-item-head clearfix">
                                                    <div class="pull-left m-r-1">
                                                        <span class="fa-stack fa-lg">
                                                            <i class="fa fa-circle fa-stack-2x text-info"></i>
                                                            <i class="fa fa-commenting fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                    </div>
                                                    <!-- Timeline - Header & Subtitle -->
                                                    <div class="user-detail">
                                                        <h5>Invitation Sent!</h5>
                                                        <p class="m-t-0 m-b-0 small text-uppercase">
                                                            <strong>
                                                                <span>From: Van Sporer</span>
                                                            </strong>
                                                        </p>
                                                    </div>
                                                </div>
                                                <!-- Timeline - START Content -->
                                                <div class="timeline-item-content m-b-0">
                                                    <p class="m-b-1">
                                                        <span>Voluptatem ab saepe iure voluptate reprehenderit ut.</span>
                                                    </p>
                                                    <p>
                                                        <button class="btn btn-default btn-sm" type="button">Accept</button>
                                                        <button class="btn btn-default btn-sm" type="button">Reject</button>
                                                    </p>
                                                </div>
                                                <!-- Timeline - END Content -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Timeline - Post -->

                                    <!-- START Timeline - Post -->
                                    <div class="timeline timeline-datetime">
                                        <div class="timeline-item p-r-1">
                                            <!-- Timeline - Mini Icon -->
                                            <div class="timeline-icon">
                                                <i class="fa fa-circle-o text-primary"></i>
                                            </div>
                                            <div class="timeline-item-inner">
                                                <!-- Timeline - Date -->
                                                <span class="timeline-item-time text-right">3:45 pm</span>
                                                <!-- Timeline - Icon  -->
                                                <div class="timeline-item-head clearfix">
                                                    <div class="pull-left m-r-1">
                                                        <span class="fa-stack fa-lg">
                                                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                                            <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                    </div>
                                                    <!-- Timeline - Header & Subtitle -->
                                                    <div class="user-detail">
                                                        <h5>Where are You?</h5>
                                                        <p class="m-t-0 m-b-0 small text-uppercase">
                                                            <strong>
                                                                <span>Erin.Stoltenberg10@gmail.com</span>
                                                            </strong>
                                                        </p>
                                                    </div>
                                                </div>
                                                <!-- Timeline - START Content -->
                                                <div class="timeline-item-content m-b-1">
                                                    <p class="m-b-1">
                                                        <span>Possimus omnis asperiores doloribus error corporis sit vel quaerat.</span>
                                                    </p>
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <div class="avatar">
                                                                <img class="media-object img-circle" alt="Avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/ariffsetiawan/128.jpg">
                                                            </div>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="m-t-0 m-b-0">
                                                                <span>Julia Beatty</span>
                                                            </h6>
                                                            <p class="m-t-0">
                                                                <span class="small">Replied: 30 Saturday, July, 2016</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Timeline - END Content -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Timeline - Post -->

                                </div>
                                <!-- END Tab: Activity -->

                            </div>
                            <!-- END Tabs Content -->

                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Modal: Profile -->
            <!-- START Modal: Alert Danger -->
            <div class="modal modal-danger fade modal-alert-danger" id = "modal-delete-user" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <!-- START Left Chat -->
                            <div class="media">
                                <div class="media-left">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x text-danger"></i>
                                        <i class="fa fa-exclamation fa-stack-1x fa-inverse"></i>
                                    </span>
                                </div>
                                <div class="media-body">
                                    <h4>Do You Want to Delete?</h4>
                                    <p>You will not be able to return it to a file or a folder. Think carefully because the
                                        effects are irreversible.</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- START Buttons -->
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" onclick="deleteUser();" class="btn btn-danger" data-dismiss="modal">Yes, Delete</button>
                            <!-- END Buttons -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Modal: Alert Danger -->

            <div class="modal modal-danger fade modal-alert-danger" id = "modal-delete-manager" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <!-- START Left Chat -->
                            <div class="media">
                                <div class="media-left">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x text-danger"></i>
                                        <i class="fa fa-exclamation fa-stack-1x fa-inverse"></i>
                                    </span>
                                </div>
                                <div class="media-body">
                                    <h4>Do You Want to Delete?</h4>
                                    <p>You will not be able to return it to a file or a folder. Think carefully because the
                                        effects are irreversible.</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- START Buttons -->
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" onclick="deleteManager();" class="btn btn-danger" data-dismiss="modal">Yes, Delete</button>
                            <!-- END Buttons -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal modal-danger fade modal-alert-danger" id = "modal-delete-trackmanager" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <!-- START Left Chat -->
                            <div class="media">
                                <div class="media-left">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x text-danger"></i>
                                        <i class="fa fa-exclamation fa-stack-1x fa-inverse"></i>
                                    </span>
                                </div>
                                <div class="media-body">
                                    <h4>Do You Want to Delete?</h4>
                                    <p>You will not be able to return it to a file or a folder. Think carefully because the
                                        effects are irreversible.</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- START Buttons -->
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" onclick="deletetrackManager();" class="btn btn-danger" data-dismiss="modal">Yes, Delete</button>
                            <!-- END Buttons -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal modal-danger fade modal-alert-danger" id = "modal-delete-playlistmanager" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <!-- START Left Chat -->
                            <div class="media">
                                <div class="media-left">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x text-danger"></i>
                                        <i class="fa fa-exclamation fa-stack-1x fa-inverse"></i>
                                    </span>
                                </div>
                                <div class="media-body">
                                    <h4>Do You Want to Delete?</h4>
                                    <p>You will not be able to return it to a file or a folder. Think carefully because the
                                        effects are irreversible.</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- START Buttons -->
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" onclick="deleteplaylistManager();" class="btn btn-danger" data-dismiss="modal">Yes, Delete</button>
                            <!-- END Buttons -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade modal-edit-profile" tabindex="-1" role="dialog" id = "modal-add-loginuser">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&#xD7;</span>
                            </button>
                            <h4 class="modal-title">Add User
                                
                            </h4>
                        </div>

                        <div class="modal-body">

                            <form class="form-horizontal">  
                                <div class="form-group">
                                    <label for="profile-first-name" class="col-sm-3 control-label">
                                        <span class="text-danger">*</span> User Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="username" placeholder="Name...">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="profile-last-name" class="col-sm-3 control-label">Password</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="userpassword" placeholder="Password...">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="profile-last-name" class="col-sm-3 control-label">Geo</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="usergeo" placeholder="Geo...">
                                    </div>
                                </div>
                            </form>
             
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id = "addloginManager">Save</button>
                        </div>
                    </div>
                </div>
            </div>

             <div class="modal fade modal-edit-profile" tabindex="-1" role="dialog" id = "modal-add-trackmanager">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&#xD7;</span>
                            </button>
                            <h4 class="modal-title">Add TrackManager
                                
                            </h4>
                        </div>

                        <div class="modal-body">

                            <form class="form-horizontal">  
                                <div class="form-group">
                                    <label for="profile-first-name" class="col-sm-3 control-label">
                                        <span class="text-danger">*</span>TrackTitle</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="tracktitle" placeholder="Title...">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="profile-last-name" class="col-sm-3 control-label">URL</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="urlname" placeholder="URL...">
                                    </div>
                                </div>
                            </form>
             
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id = "addtrackManager">Save</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade modal-edit-profile" tabindex="-1" role="dialog" id = "modal-add-playlistmanager">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&#xD7;</span>
                            </button>
                            <h4 class="modal-title">Add PlayListManager
                                
                            </h4>
                        </div>

                        <div class="modal-body">

                            <form class="form-horizontal">  
                                <div class="form-group">
                                    <label for="profile-first-name" class="col-sm-3 control-label">
                                        <span class="text-danger">*</span>PlayListName</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="playtitle" placeholder="Title...">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="profile-last-name" class="col-sm-3 control-label">PlayListURL</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="playurl" placeholder="URL...">
                                    </div>
                                </div>
                            </form>
             
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id = "addplaylistManager">Save</button>
                        </div>
                    </div>
                </div>
            </div>
</html>
