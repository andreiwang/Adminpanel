<!DOCTYPE html>
<html lang="en">
<!-- START Head -->

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">

    <!-- Enable responsiveness on mobile devices-->
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">

    <title>
        SPIN | Login
    </title>

    <!--START Loader -->
    <style>
        #initial-loader {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
            width: 100%;
            background: #212121;
            position: fixed;
            z-index: 10000;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            transition: opacity .2s ease-out
        }

        #initial-loader .initial-loader-top {
            display: flex;
            align-items: center;
            justify-content: space-between;
            width: 200px;
            border-bottom: 1px solid #2d2d2d;
            padding-bottom: 5px
        }

        #initial-loader .initial-loader-top>* {
            display: block;
            flex-shrink: 0;
            flex-grow: 0
        }

        #initial-loader .initial-loader-bottom {
            padding-top: 10px;
            color: #5C5C5C;
            font-family: -apple-system, "Helvetica Neue", Helvetica, "Segoe UI", Arial, sans-serif;
            font-size: 12px
        }

        @keyframes spin {
            100% {
                transform: rotate(360deg)
            }
        }

        #initial-loader .loader g {
            transform-origin: 50% 50%;
            animation: spin .5s linear infinite
        }

        body.loading {
            overflow: hidden !important
        }

        body.loaded #initial-loader {
            opacity: 0
        }
    </style>
    <!--END Loader-->

    <!-- SCSS Output -->
    <link rel="stylesheet" href="../assets/stylesheets/app.min.e0bb64e7.css">

    <!-- START Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{URL::to('/')}}/assets/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{URL::to('/')}}/assets/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{URL::to('/')}}/assets/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::to('/')}}/assets/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{URL::to('/')}}/assets/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{URL::to('/')}}/assets/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{URL::to('/')}}/assets/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{URL::to('/')}}/assets/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{URL::to('/')}}/assets/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{URL::to('/')}}/assets/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{URL::to('/')}}/assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{URL::to('/')}}/assets/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{URL::to('/')}}/assets/images/favicons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{URL::to('/')}}/assets/images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- END Favicon -->

    <!-- RSS -->
    <link rel="alternate" type="application/rss+xml" title="RSS" href="../atom.xml">

    <!-- Google Analytics -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-83862026-1', 'auto');
        ga('send', 'pageview');
    </script>

    <!-- jekyll settings -->
    <script>
        var ASSET_PATH_BASE = '../';
    </script>
</head>
<!-- END Head -->


<body class="sidebar-disabled navbar-disabled footer-disabled loading">
    <div id="initial-loader">
        <div>
            <div class="initial-loader-top">
                <img class="initial-loader-logo" src="{{URL::to('/')}}/assets/images/spin-logo-inverted.png" alt="Loader">
                <div class="loader loader--style1">
                    <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        width="30px" height="30px" viewbox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                        <g>
                            <path fill="#2d2d2d" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z">
                                <path fill="#2c97de" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0C22.32,8.481,24.301,9.057,26.013,10.047z">

                        </g>
                    </svg>
                </div>
            </div>
            <div class="initial-loader-bottom">
                Loading. Please Wait.
                <i class="fa fa-cricle" style="opacity: 0"></i>
            </div>
        </div>
    </div>

    <!-- Bower Libraries Scripts -->
    <script src="{{URL::to('/')}}/assets/vendor/js/lib.min.js"></script>

    <div class="main-wrap">
        <nav class="navigation">
            <!-- START Navbar -->
            <div class="navbar-inverse navbar navbar-fixed-top">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <a class="current navbar-brand" href="../index.html">
                            <img alt="Spin Logo Inverted" class="h-20" src="../assets/images/spin-logo-inverted-@2X.png">
                        </a>
                        <button class="action-right-sidebar-toggle navbar-toggle collapsed" data-target="#navdbar" data-toggle="collapse" type="button">
                            <i class="fa fa-fw fa-align-right text-white"></i>
                        </button>
                        <button class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button">
                            <i class="fa fa-fw fa-user text-white"></i>
                        </button>
                        <button class="action-sidebar-open navbar-toggle collapsed" type="button">
                            <i class="fa fa-fw fa-bars text-white"></i>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="navbar">

                        <!-- START Search Mobile -->
                        <div class="form-group hidden-lg hidden-md hidden-sm">
                            <div class="input-group hidden-lg hidden-md">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fa fa-fw fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <!-- END Navbar -->

        </nav>

        <div class="content">
            <div class="container-fluid">
                <a class="btn btn-default m-t-2 m-b-1 action-navigate-back" href="#" role="button">
                    <i class="fa fa-angle-left m-r-1"></i> Back</a>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="panel panel-default b-a-2 no-bg b-gray-dark">
                            <div class="panel-heading text-center">
                                <a href="../index.html">
                                    <img src="../assets/images/spin-logo-inverted-@2X.png" alt="Logo" class="m-t-3 m-b-3 h-20">
                                </a>
                            </div>
                            <div class="panel-body">
                                <h2 class="text-center f-w-300 m-b-0"> Login</h2>
                                <p class="text-center m-b-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                <form class="m-t-3" action="{{ route('login') }}" method="POST" >
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Enter a Username or Email..." name="email" value="{{ old('email') }}" required autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Your Password..."  name="password" required>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Password?
                                        </label>
                                    </div>
                                    <a href="../index.html" role="button" class="btn btn-block m-b-2 btn-primary">Login</a>
                                </form>
                            </div>
                            <div class="panel-footer b-a-0 b-r-a-0">
                                <a href="{{ route('password.request') }}">Forgot Password?</a>
                            </div>
                        </div>
                        <p class="text-gray-light text-center">
                            <strong>SPIN Dashboard </strong>
                            <span class="text-gray-light">&#xA9; 2009 - 2016. Made by
                                <i class="fa fa-fw fa-heart text-danger"></i> New York, US</span>
                        </p>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <script>
        // Hide loader
        (function () {
            var bodyElement = document.querySelector('body');
            bodyElement.classList.add('loading');

            document.addEventListener('readystatechange', function () {
                if (document.readyState === 'complete') {
                    var bodyElement = document.querySelector('body');
                    var loaderElement = document.querySelector('#initial-loader');

                    bodyElement.classList.add('loaded');
                    setTimeout(function () {
                        bodyElement.removeChild(loaderElement);
                        bodyElement.classList.remove('loading', 'loaded');
                    }, 200);
                }
            });
        })();
    </script>

    <!-- Bower Libraries Styles -->
    <link rel="stylesheet" href="{{URL::to('/')}}/assets/vendor/css/lib.min.css">

    <script src="{{URL::to('/')}}/assets/javascript/app.min.13a3a368.js"></script>

    <script src="{{URL::to('/')}}/assets/javascript/plugins-init.js"></script>
    <script src="{{URL::to('/')}}/assets/javascript/switchery-settings.js"></script>
</body>

</html>