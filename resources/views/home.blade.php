@extends('layouts.dashboard')


@section('content')
    <div class="content">        
            <!-- START Sub-Navbar with Header only-->
            <div class="sub-navbar sub-navbar__header">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header m-t-0">
                                <h3 class="m-t-0">System</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Sub-Navbar with Header only-->

            <!-- START Sub-Navbar with Header and Breadcrumbs-->
            <div class="sub-navbar sub-navbar__header-breadcrumbs">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 sub-navbar-column">
                            <div class="sub-navbar-header">
                                <h3>System</h3>
                            </div>
                            <ol class="breadcrumb navbar-text navbar-right no-bg">
                                <li class="current-parent">
                                    <a class="current-parent" href="../index.html">
                                        <i class="fa fa-fw fa-home"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Start
                                    </a>
                                </li>
                                <li class="active">System</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Sub-Navbar with Header and Breadcrumbs-->


            <div class="container">

                <!-- START EDIT CONTENT -->
                <div class="row">
                    <div class="col-lg-12 m-t-2">

                        <!-- START 4 Boxes -->
                        <div class="row">
                            <div class="col-lg-3 col-sm-6">
                                <div class="panel panel-default b-a-0 bg-gray-dark">
                                    <div class="panel-heading bg-primary-i">
                                        <div class="media">
                                            <div class="media-body">
                                                <span class="text-uppercsase">Active Clients</span>
                                                <br>
                                                <h1 class="display-4 m-t-0 m-b-0">
                                                    {{$count}}      
                                                    <small class="text-uppercase text-white">clients </small>
                                                    <!-- <span class="label label-white label-outline m-l-1 f-25"> -->
                                                        <!-- <i class="fa fa-caret-down"></i> 12 %</span> -->
                                                </h1>
                                            </div>
                                            <div class="media-right">
                                                <p class="data-attributes m-b-0">
                                                    <span data-peity="{ &quot;fill&quot;: [&quot;#FFFFFF&quot;, &quot;#4ca8e1&quot;],  &quot;innerRadius&quot;: 20, &quot;radius&quot;: 28  }">2/7</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body p-t-3 p-b-0 text-center">
                                        <div class="center-block">
                                            <span class="sparkline-bar">0:2,2:4,4:2,4:1,4:1,5:2,1:2,4:1,0:2,2:4,4:2,4:1,4:1,5:2,1:2,4:1</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="panel panel-default b-a-0 bg-gray-dark">
                                    <div class="panel-heading bg-success-i">
                                        <div class="media">
                                            <div class="media-body">
                                                <span class="text-uppercsase">Daily Streams</span>
                                                <br>
                                                <h1 class="display-4 m-t-0 m-b-0">{{$counttrack}}
                                                    <small class="text-uppercase text-white">tracks</small>

                                                    <!-- <span class="label label-white label-outline m-l-1 f-25">
                                                        <i class="fa fa-caret-up"></i> 2 %</span> -->

                                                </h1>
                                            </div>
                                            <div class="media-right">
                                                <p class="data-attributes m-b-0">
                                                    <span data-peity="{ &quot;fill&quot;: [&quot;#FFFFFF&quot;, &quot;#98be68&quot;],  &quot;innerRadius&quot;: 20, &quot;radius&quot;: 28  }">8/10</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body p-t-3 p-b-0 text-center">
                                        <div class="center-block">
                                            <span class="sparkline-bar">
                                                4:1,4:1,5:2,1:2,4:1,0:2,2:4,4:2,4:1,4:1,5:2,1:2,4:1,0:2,2:4,4:2
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="panel panel-default b-a-0 bg-gray-dark">
                                    <div class="panel-heading bg-warning-i">
                                        <div class="media">
                                            <div class="media-body">
                                                <span class="text-uppercsase">Daily Earnings</span>
                                                <br>
                                                <h1 class="display-4 m-t-0 m-b-0">
                                                    @php
                                                        $budget = $counttrack * 0.003
                                                    @endphp
                                                    {{$budget}}
                                                    <small class="text-uppercase text-white">$</small>

                                                    <span class="label label-white label-outline m-l-1 f-25">
                                                        <i class="fa fa-caret-down"></i> 7 %</span>

                                                </h1>
                                            </div>
                                            <div class="media-right">
                                                <p class="data-attributes m-b-0">
                                                    <span data-peity="{ &quot;fill&quot;: [&quot;#FFFFFF&quot;, &quot;#ea825c&quot;], &quot;innerRadius&quot;: 20, &quot;radius&quot;: 28 }">3/10</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body p-t-3 p-b-0 text-center">
                                        <div class="center-block">
                                            <span class="sparkline-bar">4:1,4:1,5:2,1:2,4:1,0:2,2:4,4:2,4:1,4:1,5:2,1:2,4:1,0:2,2:4,4:2</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="panel panel-default b-a-0 bg-gray-dark">
                                    <div class="panel-heading bg-danger-i">
                                        <div class="media">
                                            <div class="media-body">
                                                <span class="text-uppercsase">Current Errors/Issues</span>
                                                <br>
                                                <h1 class="display-4 m-t-0 m-b-0">{{$errorcount}}
                                                    <!-- <small class="text-uppercase text-white">kb</small>

                                                    <span class="label label-white label-outline m-l-1 f-25">
                                                        <i class="fa fa-caret-down"></i> 9 %</span> -->

                                                </h1>
                                            </div>
                                            <div class="media-right">
                                                <p class="data-attributes m-b-0">
                                                    <span data-peity="{ &quot;fill&quot;: [&quot;#FFFFFF&quot;, &quot;#d35b66&quot;], &quot;innerRadius&quot;: 20, &quot;radius&quot;: 28 }">3/7</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body p-t-3 p-b-0 text-center">
                                        <div class="center-block">
                                            <span class="sparkline-bar">2:4,4:2,4:1,4:1,5:2,1:2,4:1,0:2,2:4,4:2,4:1,4:1,5:2,1:2,4:1,0:2</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END 4 Boxes -->
                       
                        <!-- START ROW -->
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="panel panel-default no-bg b-a-2 b-gray-dark">

                                    <div class="panel-heading">
                                        <div class="btn-group pull-right" role="group" aria-label="...">
                                            <button type="button" class="btn btn-sm btn-default">
                                                <i class="fa fa-angle-left"></i>
                                            </button>
                                            <button type="button" class="btn btn-sm btn-default">
                                                <i class="fa fa-angle-right"></i>
                                            </button>
                                        </div>
                                        <h4 class="m-t-0 m-b-0">List of Clients</h4>
                                        

                                    </div>

                                    <!-- <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maiores, sunt explicabo
                                            sapiente aspernatur possimus quas ipsam amet error! Repudiandae minima minus
                                            nemo neque quae quas impedit itaque nobis dignissimos.</p>
                                    </div> -->

                                    <!-- START Table -->
                                   
                                    <!-- END Table -->
                                    <!-- <div class="hr-text hr-text-left m-t-3 m-b-1">
                                        <h6 class="text-white"><strong>List of Clients</strong></h6>
                                    </div> -->
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="small text-muted text-uppercase"><strong>Ip</strong>
                                                    </th>
                                                    <th class="small text-muted text-uppercase"><strong>Geo-Flag</strong>
                                                    </th>
                                                    <th class="small text-muted text-uppercase"><strong>Progress</strong>
                                                    </th>
                                                    <th class="small text-muted text-uppercase text-right"><strong>Runtime/Total</strong>
                                                    </th>
                                                    <th class="small text-muted text-uppercase text-right"><strong>Runtime State</strong>
                                                    </th>
                                                    <th class="small text-muted text-uppercase text-right"><strong>Actions</strong>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($clientinfo as $client)
                                                <tr>
                                                    <td class="v-a-m">
                                                        <span class="text-white">Client{{$client['id']}}</span>
                                                        <span>({{$client['ip_addr']}})</span>
                                                    </td>
                                                    <td class="v-a-m"><span class="m-r-1">{{$client['country_name']}}</span>
                                                    </td>
                                                    <td class="v-a-m">
                                                        <!-- <div class="progress m-t-0 m-b-0 b-r-a-0 h-3">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:20%">
                                                                <span>60% Complete</span>
                                                            </div>
                                                        </div> -->
                                                        <span class="m-r-1">{{$client['session_progress'] *100}}%</span>
                                                    </td>
                                                    <td class="v-a-m text-right">
                                                        <span class="text-white"><span>{{$client['runtime']}}</span>/ <span>{{$client['totaltime']}}</span>
                                                    </td>
                                                    <td class="v-a-m text-right">{{$client['status']}} <i class="fa fa-fw fa-check-circle text-success"></i>
                                                    </td>
                                                    <td class="text-right v-a-m">
                                                        <div class="dropdown">
                                                            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                <i class="fa fa-gear m-r-1"></i> <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="tasks-list.html"><i class="fa fa-fw fa-folder-open text-gray-lighter m-r-1"></i> View</a></li>
                                                                <li><a href="tasks-details.html"><i class="fa fa-ticket text-gray-lighter fa-fw m-r-1"></i> Add Task</a></li>
                                                                <li><a href="files-list.html"><i class="fa fa-fw fa-paperclip text-gray-lighter m-r-1"></i> Add Files</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="javascript: void(0)" data-toggle="modal" data-target=".modal-alert-danger"><i class="fa fa-fw fa-trash text-gray-lighter m-r-1"></i> Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END ROW -->

                    </div>
                    
                
                   
                </div>
                <!-- END EDIT CONTENT -->
                
            </div>
            <script src="{{URL::to('/')}}/assets/vendor/js/jquery.sparkline.min.js"></script>
            <script src="{{URL::to('/')}}/assets/vendor/js/highstock.min.js"></script>
            <script src="{{URL::to('/')}}/assets/javascript/peity-settings.js"></script>
            <script src="{{URL::to('/')}}/assets/javascript/sparklines-settings.js"></script>
            <script src="{{URL::to('/')}}/assets/javascript/highchart-themes/highcharts&amp;highstock-theme.js"></script>
            <script src="{{URL::to('/')}}/assets/javascript/highchart-themes/highcharts-settings.js"></script>

    </div>
    @push('footer_script')
        <script>
            // Hide loader
            (function () {
                var bodyElement = document.querySelector('body');
                bodyElement.classList.add('loading');

                document.addEventListener('readystatechange', function () {
                    if (document.readyState === 'complete') {
                        var bodyElement = document.querySelector('body');
                        var loaderElement = document.querySelector('#initial-loader');

                        bodyElement.classList.add('loaded');
                        setTimeout(function () {
                            bodyElement.classList.remove('loading', 'loaded');
                        }, 200);
                    }
                });
            })();
        </script>
    @endpush
    @include('sections.script_section1')
@endsection

    

        