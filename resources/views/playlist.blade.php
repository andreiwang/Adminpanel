@extends('layouts.dashboard')

@section('content')
        <script src="{{URL::to('/')}}/assets/javascript/modaloperation.js"></script>
        <div class="content">
            <div class="sub-navbar sub-navbar__header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-header m-t-0">
                            <h3 class="m-t-0">Users List</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Sub-Navbar with Header only-->

        <!-- START Sub-Navbar with Header and Breadcrumbs-->
            <div class="sub-navbar sub-navbar__header-breadcrumbs" style = "padding-top: 50px">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 sub-navbar-column">
                            <div class="sub-navbar-header">
                                <h3>Playlist_Manager</h3>
                            </div>
                            <ol class="breadcrumb navbar-text navbar-right no-bg">
                                <li class="current-parent">
                                    <a class="current-parent" href="{{URL::to('/home')}}">
                                        <i class="fa fa-fw fa-home"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Apps
                                    </a>
                                </li>
                                <li class="active">Playlist_Manager</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>


            <div class="container">
                <!-- START EDIT CONTENT -->

                <div class="row m-t-2">

                    <!-- START Left Column -->
                    
                    <!-- END Left Column -->

                    <div class="col-lg-12">

                        <!-- START Header with Option -->
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-5 col-xs-12">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fa fa-fw fa-search"></i>
                                        </button>
                                    </span>
                                </div>

                                <div class="btn-toolbar pull-right hidden-lg hidden-md hidden-sm m-b-2 m-t-2">
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <a class="btn btn-block btn-default" href="#" role="button">
                                            <i class="fa fa-folder"></i>
                                        </a>
                                        <a class="btn btn-block btn-default" href="#" role="button">
                                            <i class="fa fa-link"></i>
                                        </a>
                                        <a class="btn btn-block btn-default" href="#" role="button">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        <a href="users-list.html" class="btn btn-block btn-default" role="button">
                                            <i class="fa fa-list"></i>
                                        </a>
                                        <a class="btn btn-block btn-default" href="users-grid.html" role="button">
                                            <i class="fa fa-th-large"></i>
                                        </a>
                                        <a class="btn btn-block btn-default" href="#" role="button">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>

                                </div>

                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 col-sm-offset-2 col-lg-offset-3 col-md-offset-1 col-sm-4 col-sm-offset-2 hidden-xs">
                                <!-- START Toolbar -->
                                <div class="btn-toolbar pull-right">

                                    <div class="btn-group" role="group" aria-label="...">   
                                        <a class="btn btn-default" role="button" data-toggle="modal" data-target="#modal-delete-playlistmanager">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>

                                    
                                    <div class="btn-group" role="group" aria-label="...">
                                        <button type="button" class="btn btn-primary" data-target="#modal-add-playlistmanager" data-toggle="modal">
                                            <i class="fa fa-fw fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- END Toolbar -->
                            </div>
                        </div>
                        <!-- END Header with Option -->

                        <!-- START Table Users -->
                        <div class="table-responsive">
                            <table class="table table-hover m-t-3" id="user-table">

                                <!-- START Head Table -->
                                <thead>
                                    <tr>
                                        <th class="small text-muted text-uppercase" style="width: 5px">
                                            <strong>#</strong>
                                        </th>
                                        <th class="small text-muted text-uppercase" style="width: 25px">
                                            <strong>id</strong>
                                        </th>
                                        <th class="small text-muted text-uppercase">
                                            <strong>PlaylistName</strong>
                                        </th>
                                        <th class="small text-muted text-uppercase">
                                            <strong>PlayListURL</strong>
                                        </th>
                                    </tr>
                                </thead>
                                <!-- END Head Table -->

                                <tbody>
                                   @foreach($playmanagers as $playmanager)
                                    
                                    <!-- START Row -->
                                    <tr>
                                        <td class="v-a-m">
                                            <label class="m-t-0">
                                                <input type="checkbox" value="{{$playmanager->id}}">
                                            </label>
                                        </td>
                                        <td class="v-a-m">
                                            <span>{{$playmanager->id}}</span>
                                        </td>
                                        <td class="v-a-m">
                                            <span>{{$playmanager->playtitle}}</span>
                                        </td>
                                        <td class="v-a-m">
                                            <span class="label label-success label-outline">{{$playmanager->playurl}}</span>
                                        </td>
                                    </tr>
                                    <!-- END Row -->
                                   @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- END Table Users -->

                        <!-- START Pagination -->
                        <div class="text-center">
                            
                        </div>
                        <!-- START Pagination -->

                        <!-- include _modals -->

                    </div>

                </div>
                <!-- END EDIT CONTENT -->
            </div>


            <!-- START All Modals -->
            

           

            <!-- START Modal: Profile -->
            

            <script src="{{URL::to('/')}}/assets/vendor/js/holder.min.js"></script>

        </div>

 
        <!-- START Footer -->
    

    
@push('footer_script')
        <script>
            // Hide loader
            (function () {
                var bodyElement = document.querySelector('body');
                bodyElement.classList.add('loading');

                document.addEventListener('readystatechange', function () {
                    if (document.readyState === 'complete') {
                        var bodyElement = document.querySelector('body');
                        var loaderElement = document.querySelector('#initial-loader');

                        bodyElement.classList.add('loaded');
                        setTimeout(function () {
                            bodyElement.classList.remove('loading', 'loaded');
                        }, 200);
                    }
                });
            })();
        </script>
@endpush
    @include('sections.script_section1')
@endsection
