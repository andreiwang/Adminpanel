<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');


Route::post('/addUser',"ProfileController@createUser");
Route::post('/deleteUser','ProfileController@deleteUser');
Route::post('/editUser','ProfileController@editUser');
Route::post('/addloginManager','ProfileController@addloginManager');
Route::post('/deleteManager','ProfileController@deleteManager');
Route::post('/deleteUser','ProfileController@deleteManager');
Route::post('/updateManager','ProfileController@updateManager');
Route::post('/addtrackManager','ProfileController@addtrackManager');
Route::post('/deletetrackManager','ProfileController@deletetrackManager');

Route::post('/addplaylistManager','ProfileController@addplaylistManager');
Route::post('/deleteplaylistManager','ProfileController@deleteplaylistManager');

Route::get('logout',"Auth\LoginController@logout");


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/browser and map','HomeController@browser')->name('browser');
Route::get('/userlist','HomeController@userlist')->name('userlist');
Route::get('/login-manager','HomeController@loginmanager')->name('loginmanager');
Route::get('/track-manager','HomeController@trackmanager')->name('trackmanager');
Route::get('/playlist-manager','HomeController@playlistmanager')->name('playlistmanager');